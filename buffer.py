# -*- coding: utf8 -*-
from selenium import webdriver
import unittest
from page.PageGoogle import PageGoogle
from page.PageClipBoard import PageClipBoard
from BaseMethod import BaseMethod


class TestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.page_clip_board = PageClipBoard()
        self.base_method = BaseMethod()
        self.page_google = PageGoogle()

    def test_buffer_clipboard(self):
        driver = self.driver
        driver.get("https://clipboardjs.com/")
        self.page_clip_board.click_button_copy(driver)
        str_buffer_clip = self.base_method.get_buffer_data()
        str_field_clip = self.page_clip_board.set_text(driver)
        self.assertEqual(str_buffer_clip, str_field_clip)

    def test_buffer_google(self):
        driver = self.driver
        driver.get("https://google.com/")
        self.page_google.set_focus(driver)
        self.base_method.set_buffer_data()
        str_buffer_google = self.base_method.get_buffer_data()
        self.page_google.click_ctrl_v(driver)
        str_field_google = self.page_google.set_text_from_field(driver)
        self.assertEqual(str_buffer_google, str_field_google)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
