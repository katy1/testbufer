# -*- coding: utf8 -*-
import win32clipboard


class BaseMethod(object):

    def get_buffer_data(self):
        win32clipboard.OpenClipboard()
        get_data = win32clipboard.GetClipboardData(win32clipboard.CF_UNICODETEXT).encode('utf-8')
        win32clipboard.CloseClipboard()
        return get_data

    def set_buffer_data(self):
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        set_data = win32clipboard.SetClipboardText(unicode('javascript'), win32clipboard.CF_UNICODETEXT)
        win32clipboard.CloseClipboard()
        return set_data
