# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class PageGoogle(object):
    _locators = {
        'search_field': "lst-ib",
    }

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 30).until(EC.presence_of_element_located((
            By.ID, path_element)))
        return driver.find_element_by_id(path_element)

    def set_focus(self, driver):
        find_search_field = self.__find_element(
            driver, self._locators['search_field'])
        driver.execute_script(
            "return arguments[0].focus();", find_search_field)

    def click_ctrl_v(self, driver):
        self.__find_element(
            driver, self._locators['search_field']).send_keys(
            Keys.CONTROL + "v")

    def set_text_from_field(self, driver):
        text_from_search = self.__find_element(
            driver, self._locators['search_field'])
        return text_from_search.get_property("value")
