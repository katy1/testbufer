# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class PageClipBoard(object):
    _locators = {
        'copy_to_clipboard': "//*[@id='example-text']/button",
        'field_with_text': "//pre[6]//span[5]"
    }

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 30).until(EC.element_to_be_clickable((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def set_text(self, driver):
        field_text = self.__find_element(
            driver, self._locators['field_with_text'])
        return field_text.text.encode('utf-8').strip('"')

    def click_button_copy(self, driver):
        self.__find_element(
            driver, self._locators['copy_to_clipboard']).click()
